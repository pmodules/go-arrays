package array

func Reduce[acc, cur any](arrayP *[]acc, f func(cur, acc) cur, initValue cur) cur {
	_acc := initValue
	for i := 0; i < len(*arrayP); i++ {
		_acc = f(_acc, (*arrayP)[i])
	}
	return _acc
}

func Contains[T comparable](arrayP *[]T, value T) bool {
	for i := 0; i < len(*arrayP); i++ {
		if (*arrayP)[i] == value {
			return true
		}
	}
	return false
}
